INSERT INTO users(id, login, password, role)
VALUES (gen_random_uuid()::text, 'vasya', '{argon2}$argon2id$v=19$m=4096,t=3,p=1$At5Ki34yZGFGBV7B1AjznQ$IMeLd9xSieKG4q60To0g2Yz21RmkdBN5wr1l9mKZexc', 'USER'),
       (gen_random_uuid()::text, 'alex', '{argon2}$argon2id$v=19$m=4096,t=3,p=1$At5Ki34yZGFGBV7B1AjznQ$IMeLd9xSieKG4q60To0g2Yz21RmkdBN5wr1l9mKZexc', 'USER'),
       (gen_random_uuid()::text, 'petya', '{argon2}$argon2id$v=19$m=4096,t=3,p=1$At5Ki34yZGFGBV7B1AjznQ$IMeLd9xSieKG4q60To0g2Yz21RmkdBN5wr1l9mKZexc', 'SUPPORT');