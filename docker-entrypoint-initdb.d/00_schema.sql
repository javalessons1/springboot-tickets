CREATE TABLE users (
                       id TEXT PRIMARY KEY,
                       login TEXT NOT NULL,
                       password TEXT NOT NULL,
                       role TEXT NOT NULL DEFAULT 'USER'
);
CREATE TABLE tickets (
                         id TEXT PRIMARY KEY,
                         author_id TEXT NOT NULL,
                         author_name TEXT NOT NULL,
                         title TEXT NOT NULL,
                         content TEXT NOT NULL,
                         status TEXT NOT NULL DEFAULT 'OPEN'
);
CREATE TABLE comments (
                          id TEXT PRIMARY KEY,
                          ticket_id TEXT NOT NULL,
                          author_id TEXT NOT NULL,
                          author_name TEXT NOT NULL,
                          text TEXT NOT NULL
);
CREATE TABLE tokens
(
    token TEXT PRIMARY KEY,
    user_id TEXT NOT NULL REFERENCES users,
    created TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP
);