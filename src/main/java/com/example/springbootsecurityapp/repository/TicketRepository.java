package com.example.springbootsecurityapp.repository;

import com.example.springbootsecurityapp.entity.TicketEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface TicketRepository extends JpaRepository<TicketEntity, String> {
    List<TicketEntity> findByAuthorId(final String authorId);
    List<TicketEntity> findAllById(final String id);
    List<TicketEntity> findAll();
    @Modifying
    @Query("UPDATE TicketEntity t set t.status = 'closed' where id = :id")
    void setTicketStatus(String id);

    TicketEntity save(TicketEntity entity);
}
