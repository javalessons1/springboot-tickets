package com.example.springbootsecurityapp.repository;

import com.example.springbootsecurityapp.entity.CommentEntity;
import org.springframework.data.jpa.repository.JpaRepository;


import java.util.List;

public interface CommentRepository extends JpaRepository<CommentEntity, String> {
    List<CommentEntity> findAll();
    CommentEntity save(CommentEntity entity);
}
