package com.example.springbootsecurityapp.controller;

import com.example.springbootsecurityapp.dto.CommentRS;
import com.example.springbootsecurityapp.dto.TicketRS;
import com.example.springbootsecurityapp.entity.CommentEntity;
import com.example.springbootsecurityapp.entity.TicketEntity;
import com.example.springbootsecurityapp.service.TicketService;
import com.example.springbootsecurityapp.support.security.*;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Validated
@RestController
@RequestMapping("/notes")
@RequiredArgsConstructor
public class TicketController {
    private final TicketService service;

    @GetMapping("/my")
    @PreAuthorize("isAuthenticated()")
    public List<TicketRS> findByAuthorId(
            @AuthenticationPrincipal ApplicationUserDetails principal
    ) {
        return service.getByUserId(principal, principal.getId());
    }
    @GetMapping("/getAll")
    @PreAuthorize("isAuthenticated()")
    public List<TicketRS> getAll(
            @AuthenticationPrincipal ApplicationUserDetails principal
    ) {
        return service.getAll(principal);
    }

    @GetMapping("/{id}")
    @PreAuthorize("isAuthenticated()")
    public List<TicketRS> getTicketById(
            @AuthenticationPrincipal ApplicationUserDetails principal,
            @PathVariable final String id
    ) {
        return service.getByTicketId(id, principal);
    }

    @PostMapping("/{id}/close")
    @PreAuthorize("isAuthenticated()")
    public List<TicketRS> closeTicketById(
            @AuthenticationPrincipal ApplicationUserDetails principal,
            @PathVariable final String id
    ) {
        return service.setTicketStatus(id, principal);
    }
    @PostMapping("/create")
    @PreAuthorize("isAuthenticated()")
    public TicketRS createTicket(
            @AuthenticationPrincipal ApplicationUserDetails principal,
            @Valid @RequestBody TicketEntity newTicket
    ) {
        return service.save( principal, newTicket);
    }
    @PostMapping("/comment/{id}")
    @PreAuthorize("isAuthenticated()")
    public CommentRS LeftComment(
            @AuthenticationPrincipal ApplicationUserDetails principal,
            @Valid @RequestBody CommentEntity newComment,
            @PathVariable final String id
    ) {
        return service.LeftComment(id, principal, newComment);
    }
}

