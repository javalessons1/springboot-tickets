package com.example.springbootsecurityapp.support.security;

public class Roles {
  public static final String ROLE_USER = "USER";
  public static final String ROLE_ANONYMOUS = "ANONIMOUS";
  public static final String ROLE_SUPPORT= "SUPPORT";
}
