package com.example.springbootsecurityapp.mapper;

import com.example.springbootsecurityapp.dto.TicketRS;
import com.example.springbootsecurityapp.entity.TicketEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import java.util.List;

@Mapper
public abstract class TicketEntityMapper {
    public abstract List<TicketRS> toListOfNoteGetByAuthorIdRS(final List<TicketEntity> entities);

    public abstract TicketRS toNoteSaveRS(final TicketEntity saved);

    @Mapping(target = "id", source = "request.id")
    @Mapping(target = "authorId", source = "author_id")
    @Mapping(target = "authorName", source = "request.authorName")
    @Mapping(target = "title", source = "request.title")
    @Mapping(target = "content", source ="request.content")
    @Mapping(target = "status", source = "request.status")
    public abstract TicketEntity fromNoteSaveRQ(final TicketEntity request, final String author_id);

}

