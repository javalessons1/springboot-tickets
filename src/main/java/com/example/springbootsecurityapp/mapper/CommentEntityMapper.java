package com.example.springbootsecurityapp.mapper;

import com.example.springbootsecurityapp.dto.CommentRS;
import com.example.springbootsecurityapp.entity.CommentEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper
public abstract class CommentEntityMapper {

    public abstract CommentRS toCommentSaveRS(final CommentEntity saved);

    @Mapping(target = "id", source = "request.id")
    @Mapping(target = "ticketId", source = "ticketId")
    @Mapping(target = "authorId", source = "request.authorId")
    @Mapping(target = "authorName", source = "request.authorName")
    @Mapping(target = "text", source = "request.text")
    public abstract CommentEntity fromCommentSaveRQ(final CommentEntity request, final String ticketId);

}
