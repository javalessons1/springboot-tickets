package com.example.springbootsecurityapp.exception;

import org.springframework.security.authentication.BadCredentialsException;;

public class RoleException extends BadCredentialsException {
    public RoleException(String msg) {
        super(msg);
    }
}
