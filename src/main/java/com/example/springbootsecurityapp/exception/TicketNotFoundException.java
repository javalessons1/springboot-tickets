package com.example.springbootsecurityapp.exception;

import org.springframework.security.authentication.BadCredentialsException;

public class TicketNotFoundException extends BadCredentialsException {
    public TicketNotFoundException(String message) {
        super(message);
    }

}
