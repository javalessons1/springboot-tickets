package com.example.springbootsecurityapp.service;

import com.example.springbootsecurityapp.entity.UserEntity;
import com.example.springbootsecurityapp.mapper.UserEntityMapper;
import com.example.springbootsecurityapp.repository.UserRepository;
import com.example.springbootsecurityapp.support.security.*;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service
@RequiredArgsConstructor
public class UserService implements InitializingBean, UserDetailsService {
  private final UserRepository userRepository;
  private final UserEntityMapper userEntityMapper;
  private final PasswordEncoder passwordEncoder;
  private UserEntity notFoundUserEntity;

  @Override
  public void afterPropertiesSet() throws Exception {
    notFoundUserEntity = new UserEntity("-1", "notfound", passwordEncoder.encode("secret"), Roles.ROLE_ANONYMOUS);
  }
  @Override
  public UserDetails loadUserByUsername(final String username) throws UsernameNotFoundException {
    final UserEntity saved = userRepository.findByLogin(username).orElseThrow(() -> new UsernameNotFoundException(username));

    return userEntityMapper.toUserDetails(saved);
  }
}
