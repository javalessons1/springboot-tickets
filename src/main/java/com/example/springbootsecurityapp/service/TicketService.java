package com.example.springbootsecurityapp.service;

import com.example.springbootsecurityapp.dto.CommentRS;
import com.example.springbootsecurityapp.dto.TicketRS;
import com.example.springbootsecurityapp.entity.CommentEntity;
import com.example.springbootsecurityapp.entity.TicketEntity;
import com.example.springbootsecurityapp.exception.RoleException;
import com.example.springbootsecurityapp.exception.TicketNotFoundException;
import com.example.springbootsecurityapp.mapper.CommentEntityMapper;
import com.example.springbootsecurityapp.mapper.TicketEntityMapper;
import com.example.springbootsecurityapp.repository.CommentRepository;
import com.example.springbootsecurityapp.repository.TicketRepository;
import com.example.springbootsecurityapp.support.security.*;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

@Transactional
@Service
@RequiredArgsConstructor
public class TicketService {
    private final TicketRepository ticketRepository;
    private final TicketEntityMapper ticketEntityMapper;
    private final CommentRepository commentRepository;
    private final CommentEntityMapper commentEntityMapper;

    @Transactional(readOnly = true)
    public List<TicketRS> getByUserId(final ApplicationUserDetails principal, final String authorId) {
        if(!principal.getRole().equalsIgnoreCase(Roles.ROLE_USER)) {
            throw new RoleException("Not enough privileges!");
        }
        final List<TicketEntity> saved = ticketRepository.findByAuthorId(authorId);
        return ticketEntityMapper.toListOfNoteGetByAuthorIdRS(saved);
    }
    @Transactional(readOnly = true)
    public List<TicketRS> getAll(final ApplicationUserDetails principal) {
        if(!principal.getRole().equalsIgnoreCase(Roles.ROLE_SUPPORT)) {
            throw new RoleException("Not enough privileges!");
        }
        final List<TicketEntity> saved = ticketRepository.findAll();
        return ticketEntityMapper.toListOfNoteGetByAuthorIdRS(saved);
    }
    @Transactional(readOnly = true)
    public List<TicketRS> getByTicketId(final String id, final ApplicationUserDetails principal) {
        try {
            final List<TicketEntity> saved = ticketRepository.findAllById(id);
            if (!principal.getId().equalsIgnoreCase(saved.get(0).getAuthorId())) {
                if (!principal.getRole().equalsIgnoreCase(Roles.ROLE_SUPPORT)) {
                    throw new RoleException("Not enough privileges!");
                }
            }
            return ticketEntityMapper.toListOfNoteGetByAuthorIdRS(saved);
        }
        catch (Exception e) {
            throw new TicketNotFoundException("No such ticket!");
        }
    }
    @Transactional
    public List<TicketRS> setTicketStatus(final String id, final ApplicationUserDetails principal){
        final List<TicketEntity> saved = ticketRepository.findAllById(id);
        try {
            if (!principal.getId().equalsIgnoreCase(saved.get(0).getAuthorId())) {
                throw new RoleException("Not enough privileges!");
            }
            ticketRepository.setTicketStatus(id);
            return ticketEntityMapper.toListOfNoteGetByAuthorIdRS(ticketRepository.findAllById(id));
        }
        catch (Exception e) {
            throw new TicketNotFoundException("No such ticket!");
        }
    }
    @Transactional(readOnly = true)
    public CommentRS LeftComment(final String id, final ApplicationUserDetails principal, final CommentEntity request ){
        if(!principal.getRole().equalsIgnoreCase(Roles.ROLE_SUPPORT)) {
            throw new RoleException("Not enough privileges!");
        }
        final CommentEntity ticket = new CommentEntity(String.valueOf(UUID.randomUUID()),id, principal.getId(), principal.getUsername(), request.getText());
        final CommentEntity entity = commentEntityMapper.fromCommentSaveRQ(ticket, principal.getId());
        final CommentEntity saved = commentRepository.save(entity);
        return commentEntityMapper.toCommentSaveRS(saved);
    }
    @Transactional(readOnly = true)
    public TicketRS save(final ApplicationUserDetails principal, final TicketEntity request) {
        if(!principal.getRole().equalsIgnoreCase(Roles.ROLE_USER)) {
            throw new RoleException("Not enough privileges!");
        }
        final TicketEntity ticket = new TicketEntity(String.valueOf(UUID.randomUUID()), principal.getId(), principal.getUsername(), request.getTitle(), request.getContent(), "OPEN");
        final TicketEntity entity = ticketEntityMapper.fromNoteSaveRQ(ticket, principal.getId());
        final TicketEntity saved = ticketRepository.save(entity);
        return ticketEntityMapper.toNoteSaveRS(saved);
    }
}

