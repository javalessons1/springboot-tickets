package com.example.springbootsecurityapp.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Table(name = "tickets")
@Entity
public class TicketEntity {
    @Id
    private String id;
    @Column(nullable = false, columnDefinition = "TEXT")
    private String authorId;
    @Column(nullable = false, columnDefinition = "TEXT")
    private String authorName;
    @Column(nullable = false, columnDefinition = "TEXT")
    private String title;
    @Column(nullable = false, columnDefinition = "TEXT")
    private String content;
    @Column(nullable = false, columnDefinition = "TEXT")
    private String status;
}
