package com.example.springbootsecurityapp.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Table(name = "comments")
@Entity
public class CommentEntity {
    @Id
    private String id;
    @Column(nullable = false, columnDefinition = "TEXT")
    private String ticketId;
    @Column(nullable = false, columnDefinition = "TEXT")
    private String authorId;
    @Column(nullable = false, columnDefinition = "TEXT")
    private String authorName;
    @Column(nullable = false, columnDefinition = "TEXT")
    private String text;
}
