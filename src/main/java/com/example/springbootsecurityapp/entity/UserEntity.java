package com.example.springbootsecurityapp.entity;

import lombok.*;
import javax.persistence.*;

@NoArgsConstructor
@AllArgsConstructor
@ToString
@Getter
@Setter
@Table(name = "users")
@Entity
public class UserEntity {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private String id;
  @Column(nullable = false, updatable = false, unique = true, columnDefinition = "TEXT")
  private String login;
  @Column(nullable = false, columnDefinition = "TEXT")
  private String password;
  private String role;
}
